import "../CSS/headerBackground.css";

const HeaderBackground = () => {
  return (
    <div className="header-container">
      <div className="shadow">
        <div className="container">
          <div className="header-content">
            <h1>Our company offers top-notch solutions</h1>
            <p>Do not just trust us, try it yourself</p>
            <a href="#offerSection">
              <button className="offer-button">Offer</button>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};
export default HeaderBackground;
