import "../CSS/specialist.css";

const Specialist = (props) => {
  return (
    <div className="specialist-container">
      <img src={props.photoUrl} className="profile" alt="Jonny Nowak"></img>
      <div className="specialist-description">
        <h2>
          {props.name} ({props.position})
        </h2>
        <p>{props.about}</p>
      </div>
    </div>
  );
};
export default Specialist;
