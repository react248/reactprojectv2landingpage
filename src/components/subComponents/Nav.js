import "../CSS/nav.css";

const Nav = () => {
  return (
    <nav className="navbar">
      <div className="container">
        <h1 className="title">Our company</h1>
        <div className="menu-wrapper">
          <ul className="menu">
            <li>
              <a href="#aboutUsSection">About us</a>
            </li>
            <li>
              <a href="#offerSection">Offer</a>
            </li>
            <li>
              <a className="disabled" href="#">
                Contact
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};
export default Nav;
