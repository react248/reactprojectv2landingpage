import "../CSS/singleOffer.css";

const SingleOffer = (props) => {
  return props.isNew ? (
    <div className="offer dot-parent">
      <div className="dot"></div>
      <div className="offer-content-wrapper">
        <h2>{props.name}</h2>
        <p>{props.price}</p>
        <p>{props.isNew ? "Brand new" : "Standard but excellent"}</p>
      </div>
    </div>
  ) : (
    <div className="offer">
      <div className="offer-content-wrapper">
        <h2>{props.name}</h2>
        <p>{props.price}</p>
        <p>{props.isNew ? "Brand new" : "Standard but excellent"}</p>
      </div>
    </div>
  );
};
export default SingleOffer;
